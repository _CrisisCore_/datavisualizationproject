# Starting code
# Dr. Liang
# Peter Mize and Ashley Fernandez
# CSCI B466
# We worked on steps together and we read documentation to find answers to most solutions

package require vtk
package require vtkinteraction

# --------- Prepare and process the data -----------------------
# Read a vtk file
#   The output of this reader is a single vtkPolyData data object
#   and it is [plate GetOutput]
vtkPolyDataReader plate
    plate SetFileName "plate.vtk"
	#Set the name of the vector data to extract
	#  The vector data can be found in the data file
	#  There are 5 sets of vector data, i.e., mode1, mode2, mode3,
	#     mode4, and mode5
	#  Here we use the vector data named as "mode1"
	plate SetVectorsName "mode1"
	
#vtkWarpVector is a filter that modifies point coordinates by moving
#  points along vector times the scale factor. Useful for showing flow
#  profiles or mechanical deformation.
vtkWarpVector warp
    warp SetInput [plate GetOutput]
	# A positive factor (0.9) means the motion is in the direction of
	#   the surface normal (i.e., positive displacement)
    warp SetScaleFactor 0.9
	# A negtive factor (-0.9) means the motion is in the opposite
	#   direction of the surface normal (i.e., negative displacement)
	#warp SetScaleFactor -0.9
	
#vtkPolyDataNormals is a filter that computes point normals for a
#  polygonal mesh
vtkPolyDataNormals normals
    normals SetInput [warp GetOutput]

#vtkVectorDot is a filter to generate scalar values from a dataset
#  The scalar value at a point is created by computing the dot product
#  between the normal and vector at that point 
#  i.e. the "displacement plots" algorithm introduced in Lecture10
vtkVectorDot color
    color SetInput [normals GetOutput]
	

# -------- generate scalar values using Elevation algorithm ------------
#Step 2: Generate color scalars using “vtkElevationFilter” which is
#        covered in Lecture08 and used in "hawaii.tcl"
#Step 3: Comment out the statements created in step 2 (but keep the code
#        for grading purpose) so the code would be the same as that
#        before step 2.
if 0 {
vtkElevationFilter colorIt
  # ################################
  #colorIt SetInput [plate GetOutput]
  #colorIt SetLowPoint 0 0 0
  #colorIt SetHighPoint 0 0 1
  #colorIt SetScalarRange 0 0 1
  # ################################
}


# -------------- Set up the lookup table(s) ------------------------
#Step 4: Create one black white color lookup table with 256 colors 
#        and add it into the pipeline
#        Lookup table is covered in Lecture08
# See slide 33/36 Lecture 8 ***
vtkLookupTable lutBW
# ################################
if 1 {
	lutBW SetHueRange 0 0
	lutBW SetSaturationRange 0 0
	lutBW SetValueRange 0 1
	lutBW SetNumberOfColors 256
    lutBW Build
#set z 255
#set m 0
  # HERE I TRIED TO USE A FOR LOOP FOR THE BLACK TO WHITE LUT 
  # SEE HERE IF THIS IS WHAT YOU WANTED FOR THE LOOP. 
#for  {set j 0} {$j <= 255.0} {incr j} {
#		set z [expr {$z + 1}]
#		set m [expr {$m - 1}]
		#set l [expr {$l + 1}]
		#Red is going from 0 to 255 blue is going from 0 to 255
		#The value 1 is the transparency
#		eval lutBR SetTableValue $j $m 0 $z 1
#}
#	}
# #################################

#Step 5: Comment out the single statement for adding the black 
#        white color table to the pipeline. Create another color
#        table which is not black and white 
vtkLookupTable lutBR
if 1 {
  # ################################
  #Creates a Blue to Red HUE
  lutBR SetHueRange 0.6667 0.0
  #default values:
  lutBR SetSaturationRange 1.0 1.0 
  lutBR SetValueRange 1.0 1.0
  lutBR SetNumberOfColors 256
  lutBR Build
  
#k is RED 
# set k 255
# l is BLUE
# set l 0
  # HERE I TRIED TO USE A FOR LOOP FOR THE BLUE TO RED LUT 
  # SEE HERE IF THIS IS WHAT YOU WANTED FOR THE LOOP. 
#for  {set j 0} {$j <= 255.0} {incr j} {	
#       set l [expr {$l + 1}]
#		set k [expr {$k - 1}]
		#set l [expr {$l + 1}]
		#Red is going from 0 to 255 blue is going from 0 to 255
		#The value 1 is the transparency
#		eval lutBR SetTableValue $j $k 0 $l 1
#}
#}
  # ################################
	
	
# --------- Mapper and Acotor of the beam plate ----------------
vtkPolyDataMapper plateMapper
# Map the tright dataset
    plateMapper SetInput [color GetOutput]
#Step 4 and step 5: Add the required lookup table to the pipeline
	#plateMapper SetScalarRange 0 255
    #plateMapper SetLookupTable lutBW
	plateMapper SetLookupTable lutBR
    plateMapper ImmediateModeRenderingOn
	#plateMapper ScalarVisibilityOff
	

	
vtkActor plateActor
	plateActor SetMapper plateMapper
#Step 10: Set the opacity “plateActor” as 0.4 or 0.5 so we 
#         can see through the beam and glyphs can be shown easily
	# ################################
	 [plateActor GetProperty] SetOpacity 0.5
	# ################################
	
#
# ---------- Create a scalar bar actor --------------------------
#Step 8: Add a scalar bar based on your color lookup table
#        (Refer to “scalarBar.tcl” and VTK online documentation)
vtkScalarBarActor scalarBar
	# ################################
	scalarBar SetLookupTable [plateMapper GetLookupTable]
    scalarBar SetTitle "Strain"
    [scalarBar GetPositionCoordinate] SetCoordinateSystemToNormalizedViewport
    [scalarBar GetPositionCoordinate] SetValue 0.1 0.01
    scalarBar SetOrientationToHorizontal
    scalarBar SetWidth 0.8
    scalarBar SetHeight 0.17

# Test the Get/Set Position 
eval scalarBar SetPosition  [scalarBar GetPosition]
	# ################################
# ----------- create the outline actor ---------------------------
#Step 1: create an outline for the beam plate
#        (Refer to "VisQuad.tcl" in Lecture06)
vtkOutlineFilter outline
	outline SetInput [plate GetOutput] 
vtkPolyDataMapper outlineMapper
	outlineMapper SetInput [outline GetOutput]
vtkActor outlineActor
	outlineActor SetMapper outlineMapper
	[outlineActor GetProperty] SetColor 1 1 1

	
# ------------ Create the vector actor ---------------------------
#Step 9: Using vtkMaskPoints, vtkConeSource, and vtkGlyph3D to create
#        a visualization consisting of oriented glyphs representing
#        the given vector field
#        (refer to “thrshldV.tcl” in Lecture10’s sample code)
vtkThresholdPoints threshold
    # ################################
	threshold SetInput [plate GetOutput]
	threshold ThresholdByUpper 100

	# ################################
vtkMaskPoints mask
    # ################################
	mask SetInput [plate GetOutput]
    mask SetOnRatio 8
	# ################################
vtkConeSource cone
    # ################################
	cone SetResolution 3
    cone SetHeight 2
    cone SetRadius 0.5

	# ################################
vtkGlyph3D cones
    # ################################
	cones SetInput [mask GetOutput] 
    cones SetSource [cone GetOutput]
    cones SetScaleFactor 1.0
    cones SetScaleModeToScaleByVector
	# ################################
vtkLookupTable lutVec
    # ################################
	lutVec SetHueRange 0.6667 0.0
    lutVec Build
	# ################################
vtkPolyDataMapper vecMapper
    # ################################
	vecMapper SetInput [cones GetOutput]
	# ################################
vtkActor vecActor
    # ################################
    vecActor SetMapper vecMapper
	# ################################
	

# ------------- Transform actors to right postions -----------------
# #Step 1: Transform the beam and the outline actors to a similar position and angle as the following figure so the vibration can be seen clearly
	# ################################
	vtkTransform transform
	transform RotateX 50.0
	transform RotateY -25.0
	transform RotateZ 180.0
	vecActor RotateX 50.0
	vecActor RotateY -25.0
	vecActor RotateZ 180.0
	plateActor SetUserMatrix [transform GetMatrix]
	plateActor SetUserTransform transform
	outlineActor SetUserMatrix [transform GetMatrix]
	outlineActor SetUserTransform transform
	# ################################


# -------- Set Renderer, RenderWindow, and WindowInteractor ---------
vtkRenderer ren1
	ren1 SetBackground 0.2 0.3 0.4
	ren1 AddActor plateActor
	ren1 AddActor outlineActor
	ren1 AddActor scalarBar
	ren1 AddActor vecActor
	# ################################
	# ***** Your code here **********
	# ################################


# ---------- Ajust the active camera if necessary --------------
	# ################################
	# ***** Your code here **********
	# ################################
	
# set thedisplay window
vtkRenderWindow renWin
	renWin SetSize 900 600
    renWin AddRenderer ren1

# Set the interactor style
vtkInteractorStyleTrackballCamera style

# Set the window interactor
vtkRenderWindowInteractor iren
    iren SetRenderWindow renWin
	iren SetInteractorStyle style
	iren AddObserver UserEvent {wm deiconify .vtkInteract}
	# Prepare for handling events. This must be called
	#   before the interactor will work.
	iren Initialize

# prevent the tk window from showing up then start the event loop
wm withdraw .

# render the image
renWin Render
after 1000


# ----------- Animate the vibration --------------------------
#Step 6: Animate the vibration in both positive and negative 
#        directions by changing the scale factor of “vtkWarpVector”
#        in [0.0, 3.0] (Call function “SetScaleFactor”)
#*********************************************************
# Using vector data "mode1"
	# ##############################
	
#for {set i 1} {$i <= 5} {incr i} {

		#Here we call the SetVectorsName function and fill the parameter as the name
		#of each part of the file in the plate.vtk file, vtk uses strings, so the reference 
		#variable i can be used within the mode string 
		
		############################# ASK ABOUT LATER
#		plate SetVectorsName mode$i
	for {set m 0} {$m < 3} {incr m} {
		warp SetScaleFactor $m
		renWin Render
		after 250
		warp SetScaleFactor -$m
		renWin Render
		after 250
	}
# }
	# ################################
#after 2000


#Step 7: Repeat step 6 for vector sets “mode2”, “mode3”, “mode4”, and “mode5” separately
# Using vector data "mode2"
	# ################################
	plate SetVectorsName "mode2"
	for {set m 0} {$m < 3} {incr m} {
		warp SetScaleFactor $m
		renWin Render
		after 250
		warp SetScaleFactor -$m
		renWin Render
		after 250
	}
	# ################################
# after 2000

# Using vector data "mode3"
	# ################################
	plate SetVectorsName "mode3"
	for {set m 0} {$m < 3} {incr m} {
		warp SetScaleFactor $m
		renWin Render
		after 250
		warp SetScaleFactor -$m
		renWin Render
		after 250
	}
	# ################################
# after 2000

# Using vector data "mode4"
	# ################################
	plate SetVectorsName "mode4"
	for {set m 0} {$m < 3} {incr m} {
		warp SetScaleFactor $m
		renWin Render
		after 250
		warp SetScaleFactor -$m
		renWin Render
		after 250
	}
	# ################################
# after 2000

# Using vector data "mode5"
	# ################################
	plate SetVectorsName "mode5"
	for {set m 0} {$m < 3} {incr m} {
		warp SetScaleFactor $m
		renWin Render
		after 250
		warp SetScaleFactor -$m
		renWin Render
		after 250
	}
	# ################################
# after 2000
